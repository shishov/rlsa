﻿using ShEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLSA
{
    class DrawableInfo
    {
        public readonly Sprite Sprite;
        public readonly Vector2 Position;
        public readonly float Rotation;
        public readonly SpriteEffects SEffect;
        public readonly Color Color;
        public readonly float Scale;
        public readonly float ZIndex;

        public DrawableInfo(
            Sprite sprite, 
            Vector2 pos, 
            float rotation, 
            SpriteEffects effect, 
            Color color, 
            float scale = 1f,
            float zIndex = 0)
        {
            Sprite = sprite;
            Position = pos;
            Rotation = rotation;
            SEffect = effect;
            Color = color;
            Scale = scale;
            ZIndex = zIndex;
        }
    }
}
