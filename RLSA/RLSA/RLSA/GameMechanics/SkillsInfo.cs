﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA.GameMechanics
{
    class SkillsInfo
    {
        public string Name;
        public int PowerBooster;
        public int EnergyBooster;
        public int ArmorBooster;
        public int AccuracyBooster;
    }
}
