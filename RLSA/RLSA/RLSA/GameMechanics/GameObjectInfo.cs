﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RLSA.GameMechanics
{
    class GameObjectInfo
    {
        public GameObjectType Type;
        public int TileX;
        public int TileY;
        public string ObjectID;

        public GameObjectInfo(GameObjectType type, int tileX, int tileY, string objectID)
        {
            Type = type;
            TileX = tileX;
            TileY = tileY;
            ObjectID = objectID;
        }
    }
}
