﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA.GameMechanics
{
    class CharacterInfo
    {
        // Character`s state
        public int Exp;
        public int Purse;

        // Character`s statistics
        public int Health;
        public int Energy;
        public int Accuracy;
    }
}
