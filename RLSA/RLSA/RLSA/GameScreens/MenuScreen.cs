﻿using ShEngine;
using Microsoft.Xna.Framework;

namespace RLSA.GameScreens
{
    class MenuScreen : Screen
    {
        public MenuScreen(RLSAGame game)
            : base (game)
        {

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
