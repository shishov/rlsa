﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA
{  
    class Transition
    {
        float currentTime;
        float startTime;
        float endTime;

        public Action TransitionComplete; 

        public float Value
        {
            get { return (currentTime - startTime) / (endTime - startTime); }
        }

        public Transition(float from, float to)
        {            
            startTime = Math.Min(from,to);
            endTime = Math.Max(from, to);
            endTime -= startTime;
            currentTime = 0;
        }        

        public void Step(float dt)
        {

            if (currentTime < endTime)
                currentTime += dt;
            else if(TransitionComplete != null)
                TransitionComplete();
        }
    }
}
