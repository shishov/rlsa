﻿using System;
using System.Collections.Generic;
using System.Linq;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RLSA.GameComponents;
using RLSA.GameObjects;
using RLSA.LevelSystem;
using SharedContent;
using ShEngine;

namespace RLSA
{
    class GameContext
    {
        public List<GameObject> GameObjects;
        public World PhysicsWorld;        
        Player Player;

        Camera cam;
        Texture2D levelTexture;
        Level level;
        Room Room;
        Transition fadeIn;
        Transition fadeOut;        

        bool OnExit(Fixture a, Fixture b, Contact contact)
        {
            ExitType exit = ExitType.None;

            if (a.Body.UserData != null && a.Body.UserData is ExitType
                && ((GameObject)b.Body.UserData).Type == GameObjectType.Player)
                exit = (ExitType)a.Body.UserData;

            if (b.Body.UserData != null && b.Body.UserData is ExitType
                && ((GameObject)a.Body.UserData).Type == GameObjectType.Player)
                exit = (ExitType)b.Body.UserData;

            if (exit == ExitType.None)
                return false;
            fadeOut = new Transition(0, Constants.LevelTransitionTime);
            fadeOut.TransitionComplete = delegate() { LoadRoom(exit); };            
            
            return false;
        }
        
        public GameContext(Level _level)
        {
            level = _level;                 
            Room = level.Rooms[0];            
            levelTexture = AssetManager.Textures[level.Asset];
            cam = new Camera();
            cam.Zoom = Constants.CameraZoom;            
            LoadRoom(ExitType.None);            
        }

        void LoadRoom(ExitType fromExit)
        {
            fadeOut = null;
            fadeIn = new Transition(0, Constants.LevelTransitionTime);
            if (fromExit != ExitType.None)
            {
                Room newRoom = null;
                
                if (fromExit == ExitType.Left)
                    newRoom = level.Rooms.Find(r => r.X == Room.X - 1 && r.Y == Room.Y);
                if (fromExit == ExitType.Right)
                    newRoom = level.Rooms.Find(r => r.X == Room.X + 1 && r.Y == Room.Y);
                if (fromExit == ExitType.Up)
                    newRoom = level.Rooms.Find(r => r.X == Room.X && r.Y == Room.Y + 1);
                if (fromExit == ExitType.Down)
                    newRoom = level.Rooms.Find(r => r.X == Room.X && r.Y == Room.Y - 1);

                if(newRoom != null)
                    Room = newRoom;
            }

            GameObjects = new List<GameObject>();
            PhysicsWorld = new World(Vector2.Zero);

            for (int i = 0; i < LevelConstants.Width; i++)
                for (int j = 0; j < LevelConstants.Height; j++)
                {
                    if (Room.Tiles[i, j] == 1 || Room.Tiles[i, j] == 5 || Room.Tiles[i, j] == 6)
                    {
                        Body body = BodyFactory.CreateRectangle(
                            this.PhysicsWorld,
                            UnitConverter.ToSimUnits(Constants.TileSize),
                            UnitConverter.ToSimUnits(Constants.TileSize),
                            Constants.Density);
                        body.Position = UnitConverter.ToSimUnits(Constants.TileCenterCoords(i, j));
                        body.BodyType = BodyType.Static;
                    }

                    if (Room.Tiles[i, j] == 2 || Room.Tiles[i, j] == 3)
                    {
                        Body body = BodyFactory.CreateRectangle(
                            this.PhysicsWorld,
                            UnitConverter.ToSimUnits(Constants.TileSize - 10),
                            UnitConverter.ToSimUnits(Constants.TileSize - 10),
                            Constants.Density,
                            UnitConverter.ToSimUnits(Constants.TileCenterCoords(i, j)));
                        body.BodyType = BodyType.Static;
                        body.FixedRotation = true;
                        body.OnCollision += OnExit;                        
                        if (i == 0) body.UserData = ExitType.Left;
                        if (i == LevelConstants.Width - 1) body.UserData = ExitType.Right;
                        if (j == 0) body.UserData = ExitType.Up;
                        if (j == LevelConstants.Height - 1) body.UserData = ExitType.Down;
                    }
                }

            Player = new Player(this, "Player");
            this.GameObjects.Add(Player);
            
            var content = GameServices.GetService<ContentManager>();
            var mobsInfo = content.Load<MonsterData[]>("Monsters/Monster").ToList();
            var itemsInfo = content.Load<ObjectData[]>("Objects/Object").ToList();              
 
            foreach (var obj in Room.Objects)
            {
                if (obj.Type == GameObjectType.Mob)
                {
                    MonsterData md = mobsInfo.Find(mi => mi.ID == obj.ObjectID);
                    var mob = Factories.MobFactory.CreateMob(this, md, UnitConverter.ToSimUnits(Constants.TileCenterCoords(obj.TileX, obj.TileY)));
                    this.GameObjects.Add(mob);
                }

                if (obj.Type == GameObjectType.BasicItem)
                {
                    ObjectData id = itemsInfo.Find(mi => mi.Name == obj.ObjectID);
                    var mob = Factories.ItemFactory.CreateItem(this, id, UnitConverter.ToSimUnits(Constants.TileCenterCoords(obj.TileX, obj.TileY)));
                    this.GameObjects.Add(mob);
                }
            }
            
            var playerBody = (Body)Player.GetComponent<SolidBody>().GetState().Value;
            playerBody.Position = UnitConverter.ToSimUnits(Constants.DoorLocation(ExitType.None));
            if (fromExit == ExitType.Down)
                playerBody.Position = UnitConverter.ToSimUnits(Constants.DoorLocation(ExitType.Up) + new Vector2(0, 50));
            if (fromExit == ExitType.Up)
                playerBody.Position = UnitConverter.ToSimUnits(Constants.DoorLocation(ExitType.Down) + new Vector2(0, -50));
            if (fromExit == ExitType.Left)
                playerBody.Position = UnitConverter.ToSimUnits(Constants.DoorLocation(ExitType.Right) + new Vector2(-50, 0));
            if (fromExit == ExitType.Right)
                playerBody.Position = UnitConverter.ToSimUnits(Constants.DoorLocation(ExitType.Left) + new Vector2(50,0));

        }

        public void Update(float dt)
        {
            Vector2 direction = new Vector2();

            if (InputManager.KeyboardState.IsKeyDown(Keys.W))
                direction += new Vector2(0, -1);

            if (InputManager.KeyboardState.IsKeyDown(Keys.S))
                direction += new Vector2(0, 1);

            if (InputManager.KeyboardState.IsKeyDown(Keys.A))
                direction += new Vector2(-1, 0);

            if (InputManager.KeyboardState.IsKeyDown(Keys.D))
                direction += new Vector2(1, 0);

            if (direction.Length() != 0)
                Player.SendMessage(new ComponentMessage(MessageType.Move, UnitConverter.ToSimUnits(direction)));

            var playerBody = (Body)Player.GetComponent<SolidBody>().GetState().Value;


            var mousePos = new Vector2(InputManager.Cursor.X, InputManager.Cursor.Y);
            mousePos = (mousePos - new Vector2(Constants.ScreenWidth / 2, Constants.ScreenHeight / 2)) / Constants.CameraZoom + cam.Pos;            
            if (InputManager.IsCursorMoved)
                Player.SendMessage(new ComponentMessage(MessageType.MouseMove, mousePos));            

            if (InputManager.MouseState.LeftButton == ButtonState.Pressed)
                Player.SendMessage(new ComponentMessage(MessageType.Shoot, UnitConverter.ToSimUnits(mousePos)));


            Vector2 d = cam.Pos - (UnitConverter.ToDisplayUnits(playerBody.Position) + Constants.CameraOffset);
            cam.Pos -= d / (d.Length() / Constants.CameraSpeed + 1);
            cam.Pos += (mousePos - cam.Pos) / 30;

            float w = Constants.ScreenWidth / Constants.CameraZoom / 2;
            float h = Constants.ScreenHeight / Constants.CameraZoom / 2;
            float x = Math.Max(cam.Pos.X, w);
            float y = Math.Max(cam.Pos.Y, h);
            x = Math.Min(x, LevelConstants.Width * Constants.TileSize - w);
            y = Math.Min(y, LevelConstants.Height * Constants.TileSize - h);
            cam.Pos = new Vector2(x, y);

            for (int i = 0; i < GameObjects.Count; i++ )
                GameObjects[i].Process(dt);

            
            foreach(var o in GameObjects.FindAll(o => o.Destroyed == true))
            {
                o.Destroy();
                GameObjects.Remove(o);
            }

            if (fadeIn != null) fadeIn.Step(dt);
            if (fadeOut != null) fadeOut.Step(dt);

            PhysicsWorld.Step(1 / 33f);
        }

        public void Draw(GameTime gameTime, SpriteBatch batch, GraphicsDevice device)
        {
            batch.Begin(SpriteSortMode.FrontToBack,
                       BlendState.AlphaBlend,
                       SamplerState.PointWrap,
                       null,
                       null,
                       null,
                       cam.get_transformation(device));

            // Draw layer1
            for (int i = 0; i < LevelConstants.Width; i++)
            for (int j = 0; j < LevelConstants.Height; j++)
            {
                // Layer1
                var rect = new Rectangle(i * Constants.TileSize,j * Constants.TileSize,Constants.TileSize,Constants.TileSize);
                Point tile = Room.Layer1[i, j];
                float zIndex = rect.Bottom / Constants.ZIndexMod / 2;
                if(tile.X >= 0)
                    batch.Draw(levelTexture,
                        rect,
                        new Rectangle(tile.X * Constants.TileSize, tile.Y * Constants.TileSize, Constants.TileSize, Constants.TileSize),
                        Color.White,
                        0,
                        Vector2.Zero,
                        SpriteEffects.None,
                        zIndex);

                // Layer2
                rect = new Rectangle(i * Constants.TileSize, (j - 1) * Constants.TileSize + Constants.TileSize / 2, Constants.TileSize, Constants.TileSize);
                tile = Room.Layer2[i, j];
                zIndex = rect.Bottom / Constants.ZIndexMod;
                if (tile.X >= 0)
                    batch.Draw(levelTexture,
                        rect,
                        new Rectangle(tile.X * Constants.TileSize, tile.Y * Constants.TileSize, Constants.TileSize, Constants.TileSize),
                        Color.White,
                        0,
                        Vector2.Zero,
                        SpriteEffects.None,
                        zIndex);
            }

            foreach (var obj in GameObjects)
            {
                var animatable = obj.GetComponent<Animatable>();
                var sanim = obj.GetComponent<SimpleAnimation>();
                var ssprite = obj.GetComponent<SimpleSprite>();
                var dropShadow = obj.GetComponent<DropShadow>();

                if (dropShadow != null)
                {
                    var di = (DrawableInfo)dropShadow.GetState().Value;
                    di.Sprite.Draw(batch, di.Position, di.Rotation, di.Scale, di.Color,
                        (di.Sprite.SourceRect.Height + di.Position.Y) / Constants.ZIndexMod);
                }

                ComponentState cs = null;
                if (animatable != null) cs = animatable.GetState();
                if (ssprite != null) cs = ssprite.GetState();
                if (sanim != null) 
                    cs = sanim.GetState();

                if (cs.Value != null)
                {
                    var di = (DrawableInfo)cs.Value;
                    di.Sprite.Draw(batch, di.Position, di.Rotation, di.Color,
                        (di.Sprite.SourceRect.Height + di.Position.Y) / Constants.ZIndexMod);
                }
            }
            batch.End();

            if (fadeIn != null)
            {
                batch.Begin();
                AssetManager.Sprites["blank"].Draw(batch, new Rectangle(0, 0, 900, 600), new Color(0, 0, 0, 1-fadeIn.Value));
                batch.End();
            }

            if (fadeOut != null)
            {
                batch.Begin();
                AssetManager.Sprites["blank"].Draw(batch, new Rectangle(0, 0, 900, 600), new Color(0, 0, 0, fadeOut.Value));
                batch.End();
            }

            batch.Begin();
            AssetManager.Sprites["aim"].Draw(batch, new Vector2(InputManager.Cursor.X, InputManager.Cursor.Y));
            batch.End();
        }

        public void AddObject(GameObject obj)
        {
            GameObjects.Add(obj);
        }
    }
}
