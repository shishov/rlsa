﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA
{
    class ComponentMessage
    {
        public MessageType Type;
        public Object Value;

        public ComponentMessage(MessageType type, Object value = null)
        {
            Type = type;
            Value = value;
        }
    }
}
