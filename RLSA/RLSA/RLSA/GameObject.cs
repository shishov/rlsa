﻿using System.Collections.Generic;

namespace RLSA
{
    class GameObject
    {        
        public readonly GameObjectType Type;
        public readonly GameObject Parent;
        public readonly GameContext Context;

        protected readonly List<Component> _components;        
        private readonly Queue<ComponentMessage> _messageQueue;

        public bool Destroyed
        {
            get { return _destroyed; }
        }
        
        public bool _destroyed;

        public GameObject(GameContext context, GameObjectType type, GameObject parent = null)
        {
            _components = new List<Component>();
            _messageQueue = new Queue<ComponentMessage>(); 
            Context = context;            
            Type = type;
            Parent = null;
        }

        public GameObjectState GetState()
        {
            var states = new List<ComponentState>(_components.Count);
            
            if (_destroyed)
                return null;

            foreach (var component in _components)
            {
                var state = component.GetState();

                if (state != null)
                    states.Add(state);
            }

            return new GameObjectState(Type, states);
        }

        public void SendMessage(ComponentMessage msg)
        {
            _messageQueue.Enqueue(msg);
        }
             
        public void Process(float dt)
        {
            if (Destroyed) return;

            SendMessage(new ComponentMessage(MessageType.Tick, dt));
            while (_messageQueue.Count > 0)
            {
                ComponentMessage msg = _messageQueue.Dequeue();

                if (msg.Type == MessageType.Destroy)
                {
                    _destroyed = true;                    
                    return;
                }

                foreach (var component in _components)
                    component.ProcessMessage(msg);
            }
        }

        public List<Component> GetComponents()
        {
            return _components;
        }

        public T GetComponent<T>()
            where T : Component
        {            
            return (T)_components.Find(c => c is T);
        }

        public void Destroy()
        {
            foreach (var component in _components)
                component.Destroy();
        }
    }
}
