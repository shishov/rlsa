﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using ShEngine;
using SharedContent;
using Microsoft.Xna.Framework.Content;

namespace RLSA.GameComponents
{
    class AI : Component
    {
        // Зависимости компонентов
        SolidBody solidBody;        

        // Переменные
        AIType AIType;
        Body body;
        SolidBody playerBody;
        Random random;
        float mobSpeed;

        public AI(GameObject owner, AIType aitype, float speed)
            : base(owner)
        {
            mobSpeed = speed;
            AIType = aitype;
            body = (Body)solidBody.GetState().Value;
            var playerObj = owner.Context.GameObjects.Find(obj => obj.Type == GameObjectType.Player);
            playerBody = (SolidBody)playerObj.GetComponent<SolidBody>();

            // Initial
            random = new Random();
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)            
                Update((float)msg.Value);            
        }

        void Update(float dt)
        {
            if (AIType == AIType.Melee)
            {
                var direction = playerBody.CenterPosition - body.Position;
                direction.Normalize();
                solidBody.Move(UnitConverter.ToSimUnits(direction * mobSpeed));
            }
            else if (AIType == AIType.Range)
            {
                var direction = playerBody.CenterPosition - body.Position;
                direction.Normalize();
                solidBody.Move(UnitConverter.ToSimUnits(-direction * mobSpeed));
                Owner.SendMessage(new ComponentMessage(MessageType.Shoot, playerBody.CenterPosition));
            }
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();            
            return true;
        }  
    }
}
