﻿using System.Collections.Generic;
using SharedContent;

namespace RLSA.GameComponents
{
    class DropItemsOnDeath : Component
    {
        List<ObjectData> itemsToDrop;

        public DropItemsOnDeath(GameObject owner, List<ObjectData> items)
            : base(owner)
        {
            itemsToDrop = items;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Death)
            {

            }
        }
    }
}
