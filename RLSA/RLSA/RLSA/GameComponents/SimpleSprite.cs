﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Graphics;

namespace RLSA.GameComponents
{
    class SimpleSprite : Component
    {
        SolidBody solidBody;
        Body body;
        Sprite sprite;
        Vector2 imageOffset;
        Transition damageTransition;
        Color color;

        public SimpleSprite(GameObject owner, string asset, Vector2 offset)
            :base(owner)
        {
            sprite = AssetManager.Sprites[asset];
            imageOffset = offset;
            body = (Body)solidBody.GetState().Value;
            color = Color.White;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)
            {
                if (damageTransition != null)
                {
                    damageTransition.Step((float)msg.Value);
                    color = new Color(damageTransition.Value, damageTransition.Value, damageTransition.Value, 1);
                }
            }

            if (msg.Type == MessageType.TakeDamage)
                damageTransition = new Transition(0, Constants.DamageFadeTime);
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();            
            return true;
        }  

        public override ComponentState GetState()
        {           
            var di = new DrawableInfo(
                sprite,
                UnitConverter.ToDisplayUnits(body.Position) + imageOffset, 
                body.Rotation, 
                SpriteEffects.None,
                color);
            return new ComponentState(ComponentStateType.DrawableInfo, di);
        }
    }
}
