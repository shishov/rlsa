﻿using ShEngine;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using SharedContent;
using Microsoft.Xna.Framework.Content;

namespace RLSA.GameComponents
{
    class Animatable : Component
    {
        SolidBody solidBody;
        LookAt lookAt;
        MouseLookAt mouseLookAt;
        Body body;        
        Vector2 imageOffset;        
        AnimationType currentAnimation;
        Transition damageTransition;
        Color color;
        Dictionary<AnimationType, SpriteAnimation> animations;

        public Animatable(GameObject owner, string asset, Vector2 offset)
            :base(owner)
        {
            imageOffset = offset;
            body = (Body)solidBody.GetState().Value;
            color = Color.White;
            LoadAnimation(asset);
        }

        void LoadAnimation(string asset)
        {
            animations = new Dictionary<AnimationType, SpriteAnimation>();
            //animations = (Dictionary<AnimationType, SpriteAnimation>)AssetManager.Objects[asset];

            var content = GameServices.GetService<ContentManager>();
            var animContainer = content.Load<AnimationContainerData>("Animations/" + asset);
            foreach (var anim in animContainer.Animations)
            {
                var type = (AnimationType)Enum.Parse(typeof(AnimationType), anim.AnimationType);
                var texture = AssetManager.Textures[anim.TextureID];
                var sprites = new List<Sprite>();
                foreach(var framestr in anim.Frames)
                {

                    string[] coords = framestr.Split(' ');
                    int x1 = Convert.ToInt32(coords[0]);
                    int y1 = Convert.ToInt32(coords[1]);
                    int x2 = Convert.ToInt32(coords[2]);
                    int y2 = Convert.ToInt32(coords[3]);
                    var sprite = new Sprite(texture, new Rectangle(x1,y1, x2,y2));
                    sprites.Add(sprite);
                }
                
                animations.Add(type, new SpriteAnimation(sprites, anim.FrameRate, anim.Repeat));
            }            
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)
            {
                float dt = (float)msg.Value;
                animations[currentAnimation].Step(dt);

                Vector2 lookat = solidBody.CenterPosition + body.LinearVelocity * 2;

                if (mouseLookAt != null)                
                    lookat = (Vector2)mouseLookAt.GetState().Value;                                    

                if (lookAt != null)
                    lookat = (Vector2)lookAt.GetState().Value;


                bool isMoving = body.LinearVelocity.Length() > 0.01f;
                if (lookat != Vector2.Zero)
                {
                    var rel = lookat - (solidBody.CenterPosition);
                    rel.Normalize();

                    if (Math.Abs(rel.X) > Math.Abs(rel.Y))
                        rel.Y = 0;
                    else
                        rel.X = 0;

                    if (isMoving)
                    {
                        if (rel.X > 0) currentAnimation = AnimationType.MoveRight;
                        if (rel.X < 0) currentAnimation = AnimationType.MoveLeft;
                        if (rel.Y > 0) currentAnimation = AnimationType.MoveDown;
                        if (rel.Y < 0) currentAnimation = AnimationType.MoveUp;
                    }
                    else
                    {
                        if(rel.Y < 0)
                            currentAnimation = AnimationType.IdleBack;
                        else
                            currentAnimation = AnimationType.IdleFront;
                    }
                }

                if (damageTransition != null)
                {
                    damageTransition.Step(dt);
                    color = new Color(damageTransition.Value, damageTransition.Value, damageTransition.Value, 1);
                }
            }

            if (msg.Type == MessageType.TakeDamage)
                damageTransition = new Transition(0, Constants.DamageFadeTime);            
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();
            lookAt = GetOwnerComponent<LookAt>();
            mouseLookAt = GetOwnerComponent<MouseLookAt>();
            return true;
        }  

        public override ComponentState GetState()
        {                        
            var di = new DrawableInfo(
                animations[currentAnimation].CurrentSprite,                
                UnitConverter.ToDisplayUnits(body.Position) + imageOffset, 
                body.Rotation, 
                SpriteEffects.None,
                color);
            return new ComponentState(ComponentStateType.DrawableInfo, di);
        }
    }
}
