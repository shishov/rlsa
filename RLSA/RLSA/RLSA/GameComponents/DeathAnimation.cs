﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using RLSA.GameObjects;
using Microsoft.Xna.Framework.Content;
using SharedContent;
using Microsoft.Xna.Framework;

namespace RLSA.GameComponents
{
    class DeathAnimation : Component
    {
        public DeathAnimation(GameObject owner)
            :base(owner)
        {
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Death)
            {
                var pos = Owner.GetComponent<SolidBody>().CenterPosition - UnitConverter.ToSimUnits(32,32);
                var content = GameServices.GetService<ContentManager>();            
                var animData = content.Load<AnimationData>("Animations/DeathAnim");
                var type = (AnimationType)Enum.Parse(typeof(AnimationType), animData.AnimationType);
                var texture = AssetManager.Textures[animData.TextureID];
                var sprites = new List<Sprite>();
                foreach(var framestr in animData.Frames)
                {
                    string[] coords = framestr.Split(' ');
                    int x1 = Convert.ToInt32(coords[0]);
                    int y1 = Convert.ToInt32(coords[1]);
                    int x2 = Convert.ToInt32(coords[2]);
                    int y2 = Convert.ToInt32(coords[3]);
                    var sprite = new Sprite(texture, new Rectangle(x1,y1, x2,y2));
                    sprites.Add(sprite);
                }
                
                var anim = new SpriteAnimation(sprites, animData.FrameRate, animData.Repeat);

                Owner.Context.AddObject(new Animation(Owner.Context,UnitConverter.ToDisplayUnits(pos),anim));
            }
        }
    }
}
