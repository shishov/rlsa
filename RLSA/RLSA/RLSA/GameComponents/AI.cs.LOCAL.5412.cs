﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using ShEngine;

namespace RLSA.GameComponents
{
    class AI : Component
    {
        // Зависимости компонентов
        SolidBody solidBody;        

        // Переменные
        AIType AIType;
        Body body;
        SolidBody playerBody;
        Random random;

        public AI(GameObject owner, AIType aitype)
            : base(owner)
        {
            AIType = aitype;
            body = (Body)solidBody.GetState().Value;
            var playerObj = owner.Context.GameObjects.Find(obj => obj.Type == GameObjectType.Player);
            playerBody = (SolidBody)playerObj.GetComponent<SolidBody>();

            // Initial
            random = new Random();
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)            
                Update((float)msg.Value);            
        }

        void Update(float dt)
        {
            var direction = new Vector2((float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1);
            solidBody.Move(UnitConverter.ToSimUnits(direction * Constants.PlayerMoveSpeed)); 
            Owner.SendMessage(new ComponentMessage(MessageType.Shoot, playerBody.CenterPosition));
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();            
            return true;
        }  
    }
}
