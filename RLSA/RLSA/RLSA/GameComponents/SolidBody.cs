﻿using Microsoft.Xna.Framework;
using FarseerPhysics;
using FarseerPhysics.Factories;
using FarseerPhysics.Dynamics;
using System;
using ShEngine;
using FarseerPhysics.Dynamics.Contacts;

namespace RLSA.GameComponents
{
    internal class SolidBody : Component
    {
        Body body;
        Vector2 simSize;

        public SolidBody(GameObject owner, Vector2 size, Vector2 position, bool fixedRotation, bool dynamic = true)
            : base(owner)
        {
            simSize = UnitConverter.ToSimUnits(size);
            body = BodyFactory.CreateRectangle(owner.Context.PhysicsWorld, 
                UnitConverter.ToSimUnits(size.X), 
                UnitConverter.ToSimUnits(size.Y), 
                Constants.Density);

            if(dynamic)
                body.BodyType = BodyType.Dynamic;
            else
                body.BodyType = BodyType.Static;
            body.Position = position - simSize / 2;
            body.FixedRotation = fixedRotation;
            body.UserData = Owner;
            body.Mass = 1;
            body.OnCollision += OnCollide;
        }

        bool OnCollide(Fixture a, Fixture b, Contact contact)
        {
            if (a.Body == body)
                Owner.SendMessage(new ComponentMessage(MessageType.Collide, b.Body.UserData));
            if (b.Body == body)
                Owner.SendMessage(new ComponentMessage(MessageType.Collide, a.Body.UserData));
            return true;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)            
                body.LinearVelocity *= 0.9f;            
        }

        public override bool Probe()
        {
            return false;
        }

        public void Move(Vector2 vec)
        {
            body.ApplyLinearImpulse(vec);            
        }

        public Vector2 CenterPosition
        {
            get { return body.Position + simSize / 2; }
        }

        public Vector2 RealSize
        {
            get { return simSize; }
        }

        public override ComponentState GetState()
        {
            return new ComponentState(ComponentStateType.SolidBodyState, body);
        }

        public override void Destroy()
        {            
            Owner.Context.PhysicsWorld.RemoveBody(body);            
        }
    }
}
