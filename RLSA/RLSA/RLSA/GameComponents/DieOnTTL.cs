﻿namespace RLSA.GameComponents
{
    internal class DieOnTTL : Component
    {
        private readonly double ttl;
        private double lifetime;

        public DieOnTTL(GameObject owner, double ttl)
            : base(owner)
        {
            this.ttl = ttl;
            lifetime = 0.0;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)
            {
                lifetime += (float)msg.Value;
                if (lifetime >= ttl)
                    Owner.SendMessage(new ComponentMessage(MessageType.Destroy, null));
            }
        }        
    }
}
