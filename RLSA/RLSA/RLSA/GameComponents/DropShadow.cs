﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ShEngine;
using Microsoft.Xna.Framework.Graphics;

namespace RLSA.GameComponents
{
    class DropShadow : Component
    {
        Sprite sprite;
        SolidBody solidBody;
        Vector2 imageOffset;
        float scale;

        public DropShadow(GameObject owner, Vector2 offset, float size)
            : base(owner)
        {
            imageOffset = offset;
            sprite = AssetManager.Sprites["shadow"];
            scale = size;
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();
            return true;
        }

        public override ComponentState GetState()
        {
            var di = new DrawableInfo(
                sprite,
                UnitConverter.ToDisplayUnits(solidBody.CenterPosition) + imageOffset,
                0,
                SpriteEffects.None,
                Color.White, scale);
            return new ComponentState(ComponentStateType.DrawableInfo, di);
        }
    }
}
