﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using ShEngine;


namespace RLSA.GameComponents
{
    /// <summary>
    /// Компонент, который следит за мышкой
    /// </summary>
    class MouseLookAt : Component
    {
        Vector2 mouseCoords;

        public MouseLookAt(GameObject owner)
            : base(owner)
        {            
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.MouseMove)            
                mouseCoords = (Vector2)msg.Value;            
        }
        public override ComponentState GetState()
        {            
            return new ComponentState(ComponentStateType.LookAtState, UnitConverter.ToSimUnits(mouseCoords));
        }
    }
}
