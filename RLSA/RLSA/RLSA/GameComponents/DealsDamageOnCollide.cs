﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA.GameComponents
{
    class DealsDamageOnCollide : Component
    {
        float _damageVal;

        public DealsDamageOnCollide(GameObject owner, float amount)
            :base(owner)
        {
            _damageVal = amount;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Collide && msg.Value is GameObject)
            {   
                var obj = (GameObject)msg.Value;
                
                if (obj != null)                
                    obj.SendMessage(new ComponentMessage(MessageType.TakeDamage, _damageVal));                
            }
        }
    }
}
