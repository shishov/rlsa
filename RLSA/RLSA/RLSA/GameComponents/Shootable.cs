﻿using Microsoft.Xna.Framework;
using RLSA.GameObjects;
using ShEngine;

namespace RLSA.GameComponents
{
    class Shootable : Component
    {
        SolidBody solidBody;
        float rate;
        float currentTime = 0;

        public Shootable(GameObject owner, float fireRate)
            :base(owner)
        {
            rate = fireRate;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)            
                currentTime += (float)msg.Value;            

            if (msg.Type == MessageType.Shoot && currentTime > rate)
            {
                currentTime = 0;
                var target = (Vector2)msg.Value;
                var r = target - solidBody.CenterPosition;
                r.Normalize();
                solidBody.Move(-r * UnitConverter.ToSimUnits(5));
                var bulletPos = solidBody.CenterPosition + r * (solidBody.RealSize.Length() + UnitConverter.ToSimUnits(2));
                Owner.Context.AddObject(new Bullet(
                    Owner.Context, 
                    bulletPos, 
                    "bullet", 
                    target, 
                    Constants.BulletSpeed, 
                    new Vector2(2, 2)));
            }
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();            
            return true;
        }          
    }
}
