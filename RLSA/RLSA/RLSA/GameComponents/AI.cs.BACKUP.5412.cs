﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using ShEngine;

namespace RLSA.GameComponents
{
    class AI : Component
    {
        // Зависимости компонентов
        SolidBody solidBody;        

        // Переменные
        AIType AIType;
        Body body;
        SolidBody playerBody;
        Random random;

        public AI(GameObject owner, AIType aitype)
            : base(owner)
        {
            AIType = aitype;
            body = (Body)solidBody.GetState().Value;
            var playerObj = owner.Context.GameObjects.Find(obj => obj.Type == GameObjectType.Player);
            playerBody = (SolidBody)playerObj.GetComponent<SolidBody>();

            // Initial
            random = new Random();
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)            
                Update((float)msg.Value);            
        }

        void Update(float dt)
        {
<<<<<<< HEAD
            var direction = new Vector2((float)random.NextDouble() * 2 - 1, (float)random.NextDouble() * 2 - 1);
            solidBody.Move(UnitConverter.ToSimUnits(direction * Constants.PlayerMoveSpeed)); 
            Owner.SendMessage(new ComponentMessage(MessageType.Shoot, playerBody.CenterPosition));
=======
            //playerBody.Position.X
            if (AIType == AIType.Melee)
            {
                var direction = playerBody.Position - body.Position;
                direction.Normalize();
                solidBody.Move(UnitConverter.ToSimUnits(direction * Constants.PlayerMoveSpeed / 2));
            }
            else if (AIType == AIType.Range)
            {
                var direction = playerBody.Position - body.Position;
                direction.Normalize();
                solidBody.Move(UnitConverter.ToSimUnits(-direction * Constants.PlayerMoveSpeed / 2));
            }
>>>>>>> 4a2dfe0b1b19673a6cfb89bb45abffea8c74a196
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();            
            return true;
        }  
    }
}
