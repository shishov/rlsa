﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA.GameComponents
{
    class Healthy : Component    
    {
        int _health;

        public Healthy(GameObject owner, int initial)
            :base(owner)
        {
            _health = initial;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.TakeDamage)
            {
                if (_health <= 0)
                    Owner.SendMessage(new ComponentMessage(MessageType.Destroy)); 
                _health -= (int)Math.Round((float)msg.Value);
                if(_health <= 0)
                    Owner.SendMessage(new ComponentMessage(MessageType.Death));                
            }
        }

        public int Health
        {
            get { return _health; }
        }
    }
}
