﻿namespace RLSA.GameComponents
{
    class DieOnCollide : Component
    {    

        public DieOnCollide(GameObject owner)
            : base(owner)
        {            
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Collide)
                Owner.SendMessage(new ComponentMessage(MessageType.Destroy));
        }
    }
}
