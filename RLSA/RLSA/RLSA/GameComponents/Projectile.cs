﻿using System;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;

namespace RLSA.GameComponents
{    
    internal class Projectile : Component
    {
        private SolidBody solidBody;
        private Body body;
        private readonly float speed;
        private Vector2 velocity;
        Vector2 _target;

        public Projectile(GameObject owner, float speed, Vector2 target)
            : base(owner)
        {
            this.speed = speed;               
            body = (Body)solidBody.GetState().Value;
            var direction = target - body.Position;
            direction.Normalize();
            velocity = direction * speed;
            _target = target;
            body.LinearVelocity = velocity;
            body.Rotation = (float)Math.Atan2(direction.Y, direction.X) + 3.1415f / 2;
            body.IsBullet = true;
            body.CollidesWith = Category.Cat1;
            body.CollisionCategories = Category.Cat10;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)
            {
                body.LinearVelocity = velocity;
                var vec = _target - body.Position;
                //body.Rotation = (float)Math.Atan2(vec.Y, vec.X);
            }
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();
            return true;
        }        
    }
}
