﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;

namespace RLSA.GameComponents
{
    /// <summary>
    /// Компонент, который смотрит на другой объект
    /// </summary>
    class LookAt : Component
    {
        SolidBody targetSolidBody;        

        public LookAt(GameObject owner, GameObject target)
            :base(owner)
        {
            targetSolidBody = target.GetComponent<SolidBody>();
        }

        public override ComponentState GetState()
        {   
            return new ComponentState(ComponentStateType.LookAtState, targetSolidBody.CenterPosition);
        }
    }
}
