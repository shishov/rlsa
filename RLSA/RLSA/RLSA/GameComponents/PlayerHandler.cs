﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using Microsoft.Xna.Framework;

namespace RLSA.GameComponents
{
    class PlayerHandler : Component
    {
        private SolidBody solidBody;

        public PlayerHandler(GameObject owner)
            : base(owner)
        {
            Probe();
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Move)
            {
                Vector2 direction = (Vector2)msg.Value;
                direction.Normalize();
                solidBody.Move(UnitConverter.ToSimUnits(direction * Constants.PlayerMoveSpeed));                
            }

            if (msg.Type == MessageType.Shoot){}
            if (msg.Type == MessageType.Interact){}
        }

        public override bool Probe()
        {
            solidBody = GetOwnerComponent<SolidBody>();
            return true;
        }  
    }
}
