﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RLSA.GameComponents
{
    class SimpleAnimation : Component
    {
        SpriteAnimation animation;
        Vector2 position;

        public SimpleAnimation(GameObject owner, SpriteAnimation anim, Vector2 pos)
            :base(owner)
        {
            animation = anim;
            position = pos;
        }

        public override void ProcessMessage(ComponentMessage msg)
        {
            if (msg.Type == MessageType.Tick)
                animation.Step((float)msg.Value);
        }

        public override ComponentState GetState()
        {
            var di = new DrawableInfo(
                animation.CurrentSprite,
                position,
                0,
                SpriteEffects.None,
                Color.White);
            return new ComponentState(ComponentStateType.DrawableInfo, di);
        }
    }
}
