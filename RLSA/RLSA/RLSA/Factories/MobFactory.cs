﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedContent;
using Microsoft.Xna.Framework;
using RLSA.GameObjects;

namespace RLSA.Factories
{
    static class MobFactory
    {
        public static Mob CreateMob(GameContext context, MonsterData data, Vector2 position)
        {
            var mob = new Mob(
                context: context,
                animationAsset: data.Asset,
                position: position,
                size: data.Size,
                imageOffset: data.ImageOffset,
                speed: data.Speed,
                bulletID: data.BulletID,
                hp: data.HP,
                damage: data.Damage,
                fireRate: data.FireRate,
                exp: data.Exp,
                type: (AIType)data.Type
                );
            return mob;
        }
    }
}
