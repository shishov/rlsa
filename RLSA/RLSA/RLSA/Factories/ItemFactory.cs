﻿using System.Linq;
using RLSA.GameObjects;
using SharedContent;
using Microsoft.Xna.Framework;
using ShEngine;

namespace RLSA.Factories
{
    class ItemFactory
    {
        public static Item CreateItem(GameContext context, ObjectData data, Vector2 position)
        {
            var assetName = data.Name;
            if (!AssetManager.Sprites.Keys.Contains(assetName))
            {
                var tex = AssetManager.Textures["Items"];
                AssetManager.Sprites.Add(assetName, new Sprite(tex, data.CropRect));
            }
            
            var item = new Item(
                context: context,
                asset: assetName,
                position: position,
                size: data.Size,
                destructable: data.Destructable,
                hp: data.Health,
                offset: data.ImageOffset
                );
            return item;
        }
    }
}
