using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ShEngine;
using RLSA.GameScreens;
using Microsoft.Xna.Framework.Content;

namespace RLSA
{    
    public class RLSAGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;        

        public RLSAGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void Initialize()
        {
            GameServices.AddService<GraphicsDeviceManager>(graphics);
            GameServices.AddService<GraphicsDevice>(GraphicsDevice);
            GameServices.AddService<ContentManager>(Content);
            InputManager.Initialize();
            var initialScreen = new GameScreen(this);
            ScreenManager.Initialize(initialScreen);            
            base.Initialize();
        }

        protected override void LoadContent()
        {         
        }

        
        protected override void UnloadContent()
        {        
        }

        protected override void Update(GameTime gameTime)
        {
            InputManager.Update(gameTime);
            ScreenManager.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            ScreenManager.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
