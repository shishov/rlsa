﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA
{
    enum MessageType
    {        
        Death,
        Tick,
        MouseMove,
        Move,
        Interact,
        Shoot,
        TakeDamage,
        Collide,
        Destroy,
    }
}
