﻿using RLSA.GameComponents;
using Microsoft.Xna.Framework;

namespace RLSA.GameObjects
{
    class Chest : GameObject
    {
        public Chest(GameContext context, Vector2 position, string asset, Vector2 target, float speed, Vector2 size)
            : base(context, GameObjectType.Bullet)
        {
            _components.Add(new Healthy(this, 30));
        }
    }
}
