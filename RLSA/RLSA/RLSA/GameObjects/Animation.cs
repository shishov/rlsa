﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RLSA.GameComponents;
using ShEngine;

namespace RLSA.GameObjects
{
    class Animation : GameObject
    {
        public Animation(GameContext context, Vector2 position, SpriteAnimation animation)
            : base(context, GameObjectType.Animation)
        {
            _components.Add(new DieOnTTL(this, 400));
            _components.Add(new SimpleAnimation(this, animation, position));
        }
    }
}
