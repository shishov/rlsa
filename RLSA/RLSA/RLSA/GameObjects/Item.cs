﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RLSA.GameComponents;

namespace RLSA.GameObjects
{
    class Item : GameObject
    {
        public Item(GameContext context, Vector2 position, Vector2 size, string asset,Vector2 offset, bool destructable, int hp)
            : base(context, GameObjectType.BasicItem)
        {
            _components.Add(new SolidBody(this, size, position, true, false));
            _components.Add(new SimpleSprite(this, asset, offset));
            _components.Add(new DropShadow(this, new Vector2(-0.3f * 32 / 2, 0.3f * 32 / 2), 0.3f));
            _components.Add(new DeathAnimation(this)); 
            
            if(destructable)
                _components.Add(new Healthy(this, hp));           
            
        }
    }
}
