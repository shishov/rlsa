﻿using RLSA.GameComponents;
using Microsoft.Xna.Framework;

namespace RLSA.GameObjects
{
    class Player : GameObject    
    {
        public Player(GameContext context, string asset)
            : base(context, GameObjectType.Player)
        {   
            _components.Add(new SolidBody(this, new Vector2(10, 10), Vector2.Zero, true));
            _components.Add(new PlayerHandler(this));
            _components.Add(new MouseLookAt(this));   
            _components.Add(new Animatable(this, asset, new Vector2(-12,-40)));
            _components.Add(new Shootable(this, 100));
            _components.Add(new Healthy(this, 100));
            _components.Add(new DropShadow(this, new Vector2(-12, 0), 0.7f));
        }
    }
}
