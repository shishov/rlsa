﻿using RLSA.GameComponents;
using Microsoft.Xna.Framework;

namespace RLSA.GameObjects
{
    class Bullet : GameObject  
    {
        public Bullet(GameContext context, Vector2 position, string asset, Vector2 target, float speed, Vector2 size)
            : base(context, GameObjectType.Bullet)
        {
            _components.Add(new SolidBody(this, size, position, false));
            _components.Add(new SimpleSprite(this, asset, size / 2));
            _components.Add(new Projectile(this, speed, target));
            _components.Add(new DealsDamageOnCollide(this, 1));
            _components.Add(new DieOnCollide(this));
            _components.Add(new DieOnTTL(this, 1000));
            _components.Add(new DropShadow(this, new Vector2(- size.X, 10), 0.3f));
        }
    }
}
