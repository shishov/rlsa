﻿using RLSA.GameComponents;
using Microsoft.Xna.Framework;

namespace RLSA.GameObjects
{
    class Mob : GameObject
    {
        public Mob(
            GameContext context, 
            string animationAsset, 
            Vector2 position, 
            AIType type, 
            float speed, 
            int hp, 
            int damage, 
            float fireRate, 
            int bulletID, 
            int exp,
            Vector2 size,
            Vector2 imageOffset)
            : base(context, GameObjectType.Mob)
        {
            _components.Add(new SolidBody(this, size, position, true));

            var player = context.GameObjects.Find(o => o.Type == GameObjectType.Player);
            _components.Add(new LookAt(this, player));

            _components.Add(new Animatable(this, animationAsset, imageOffset));

            if(type == AIType.Range)
                _components.Add(new Shootable(this, fireRate));

            _components.Add(new AI(this, type, speed));
            _components.Add(new Healthy(this, hp));
            _components.Add(new DeathAnimation(this)); 
            _components.Add(new DealsDamageOnCollide(this, damage));
            _components.Add(new DropShadow(this, new Vector2(-0.7f * 32 / 2, 0.7f * 32 / 2), 0.7f));
            
        }
    }
}
