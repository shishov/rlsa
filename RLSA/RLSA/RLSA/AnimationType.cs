﻿namespace RLSA
{
    enum AnimationType
    {
        IdleFront,
        IdleBack,
        IdleLeft,
        IdleRight,
        
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,

        AttackUp,
        AttackDown,
        AttackLeft,
        AttackRight,

        Death,
    }
}
