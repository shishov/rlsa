﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA
{
    class ComponentState
    {
        public readonly ComponentStateType Type;
        public readonly Object Value;

        public ComponentState(ComponentStateType type, Object value)
        {
            Type = type;
            Value = value;
        }
    }
}
