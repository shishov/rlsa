﻿using Microsoft.Xna.Framework;
using RLSA.LevelSystem;

namespace RLSA
{
    static class Constants
    {
        // For physics
        public static float Density = 1f;

        // player constants
        public static float PlayerMoveSpeed = 20f;
        public static float CameraSpeed = 6f;
        public static float LevelTransitionTime = 100f;
        public static Vector2 CameraOffset = new Vector2(15, 25);
        public static float BulletSpeed = 2f;
        public static int ScreenWidth = 800;
        public static int ScreenHeight = 480;
        public static int TileSize = 32;
        public static float CameraZoom = 2.5f;
        public static float DamageFadeTime = 50;
        public static float ZIndexMod = 1000;
              
        public static Vector2 TileCoords(int x, int y)
        {
            return new Vector2(x * TileSize, y * TileSize);
        }

        public static Vector2 TileCenterCoords(int x, int y)
        {
            return new Vector2(x * TileSize + TileSize / 2, y * TileSize + TileSize / 2);
        }

        public static Vector2 DoorLocation(ExitType door)
        {
            switch (door)
            {
                case ExitType.Left:
                    return TileCenterCoords(0, LevelConstants.Height / 2);                    
                case ExitType.Right:
                    return TileCenterCoords(LevelConstants.Width - 1, LevelConstants.Height / 2);                    
                case ExitType.Up:
                    return TileCenterCoords(LevelConstants.Width / 2, 0);                    
                case ExitType.Down:
                    return TileCenterCoords(LevelConstants.Width / 2, LevelConstants.Height - 1);                    
                case ExitType.None:
                    return TileCenterCoords(LevelConstants.Width / 2, LevelConstants.Height / 2);                    
            }
            return Vector2.Zero;
        }
    }
}
