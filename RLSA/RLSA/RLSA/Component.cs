﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA
{
    internal abstract class Component
    {
        public readonly GameObject Owner;

        public Component(GameObject owner)
        {
            Owner = owner;
            Probe();
        }

        /// <summary>
        /// Processes incoming message
        /// </summary>
        /// <param name="msg"></param>
        public virtual void ProcessMessage(ComponentMessage msg) { return; }

        /// <summary>
        /// Check components
        /// </summary>
        /// <returns></returns>
        public virtual bool Probe() { return false; }

        /// <summary>
        /// Returns state of a component
        /// </summary>
        /// <returns></returns>
        public virtual ComponentState GetState() { return null; }

        public T GetOwnerComponent<T>()
            where T : Component
        {
            return (T)Owner.GetComponent<T>();
        }

        public virtual void Destroy() { return; }
    }
}
