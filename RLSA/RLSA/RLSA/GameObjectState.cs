﻿using System.Collections.Generic;

namespace RLSA
{
    class GameObjectState
    {
        public readonly GameObjectType Type;
        public readonly List<ComponentState> States;

        public GameObjectState( GameObjectType type, List<ComponentState> states)
        {
            Type = type;
            States = states;
        }
    }
}
