﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RLSA.LevelSystem
{
    enum ExitType
    {
        None,
        Up,
        Down,
        Left,
        Right,
    }
}
