﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using Microsoft.Xna.Framework.Content;
using SharedContent;
using RLSA.GameObjects;
using RLSA.GameMechanics;
using Microsoft.Xna.Framework;

namespace RLSA.LevelSystem
{
    class Room
    {
        public int X;
        public int Y;
        public int[,] Tiles;
        public Point[,] Layer1;
        public Point[,] Layer2;
        bool Top = false;
        bool Bottom = false;
        bool Right = false;
        bool Left = false;
        public string Asset;

        public List<GameObjectInfo> Objects; 

        public Room()
        {
            Tiles = new int[LevelConstants.Width, LevelConstants.Height];
            Layer1 = new Point[LevelConstants.Width, LevelConstants.Height];
            Layer2 = new Point[LevelConstants.Width, LevelConstants.Height];
        }

        public void Generate(Level level, int x, int y, string asset, int limit, ExitType previousExit)
        {
            X = x;
            Y = y;
            Asset = asset;
            List<Template> tmpList;
            Template template = null;
            if (limit <= 0)
            {
                switch (previousExit)
                {
                    case ExitType.Up:
                        tmpList = LevelConstants.Templates.FindAll(drct => drct.Bottom == true && drct.Top == false && drct.Right == false && drct.Left == false);
                        template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                        break;

                    case ExitType.Down:
                        tmpList = LevelConstants.Templates.FindAll(drct => drct.Top == true && drct.Bottom == false && drct.Right == false && drct.Left == false);
                        template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                        break;

                    case ExitType.Left:
                        tmpList = LevelConstants.Templates.FindAll(drct => drct.Right == true && drct.Top == false && drct.Left == false && drct.Bottom == false);
                        template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                        break;

                    case ExitType.Right:
                        tmpList = LevelConstants.Templates.FindAll(drct => drct.Left == true && drct.Top == false && drct.Right == false && drct.Bottom == false);
                        template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                        break;
                }
                    
                for (int col = 0; col < LevelConstants.Width; col++)
                    for (int row = 0; row < LevelConstants.Height; row++)
                    {
                        Tiles[col, row] = template.Tiles[col, row];
                    }
                if (level.Rooms.FindAll(r => r.X == x && r.Y == y + 1).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x && r.Y == y + 1).Bottom == true)
                    {
                        Tiles[7, 0] = 2;
                        Tiles[8, 0] = 3;
                        Top = true;
                    }
                    else
                    {
                        Tiles[7, 0] = 1;
                        Tiles[8, 0] = 1;
                        Top = false;
                    }
                }
                if (level.Rooms.FindAll(r => r.X == x && r.Y == y - 1).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x && r.Y == y - 1).Top == true)
                    {
                        Tiles[7, 8] = 2;
                        Tiles[8, 8] = 3;
                        Bottom = true;
                    }
                    else
                    {
                        Tiles[7, 8] = 1;
                        Tiles[8, 8] = 1;
                        Bottom = false;
                    }
                }
                if (level.Rooms.FindAll(r => r.X == x + 1 && r.Y == y).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x + 1 && r.Y == y).Left == true)
                    {
                        Tiles[15, 4] = 2;
                        Right = true;
                    }
                    else
                    {
                        Tiles[15, 4] = 1;
                        Right = false;
                    }
                }
                if (level.Rooms.FindAll(r => r.X == x - 1 && r.Y == y).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x - 1 && r.Y == y).Right == true)
                    {
                        Tiles[0, 4] = 2;
                        Left = true;
                    }
                    else
                    {
                        Tiles[0, 4] = 1;
                        Left = false;
                    }
                }                               

                // Registering room in the level
                level.Rooms.Add(this);
            }
            else
            {
                // пробивание стены если есть комната в такой локации
                if (previousExit == ExitType.None)
                {
                    int index = LevelConstants.Random.Next(LevelConstants.Templates.Count);
                    template = LevelConstants.Templates[index];
                }
                else
                {
                    switch (previousExit)
                    {
                        case ExitType.Up:
                            tmpList = LevelConstants.Templates.FindAll(drct => drct.Bottom == true);
                            template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                            break;

                        case ExitType.Down:
                            tmpList = LevelConstants.Templates.FindAll(drct => drct.Top == true);
                            template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                            break;

                        case ExitType.Left:
                            tmpList = LevelConstants.Templates.FindAll(drct => drct.Right == true);
                            template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                            break;

                        case ExitType.Right:
                            tmpList = LevelConstants.Templates.FindAll(drct => drct.Left == true);
                            template = tmpList[LevelConstants.Random.Next(tmpList.Count)];
                            break;
                    }
                }

                // Заливка по шаблону
                for (int col = 0; col < LevelConstants.Width; col++)
                    for (int row = 0; row < LevelConstants.Height; row++)
                    {
                        Tiles[col, row] = template.Tiles[col, row];

                        // Обнаруживаем двери
                        if (row == 0 && col == 7 && Tiles[col, row] == 2) Top = true;
                        if (row == 8 && col == 7 && Tiles[col, row] == 2) Bottom = true;
                        if (row == 4 && col == 0 && Tiles[col, row] == 2) Left = true;
                        if (row == 4 && col == 15 && Tiles[col, row] == 2) Right = true;
                    }
                
                if (level.Rooms.FindAll(r => r.X == x && r.Y == y + 1).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x && r.Y == y + 1).Bottom == true)
                    {
                        Tiles[7, 0] = 2;
                        Tiles[8, 0] = 3;
                        Top = true;
                    }
                    else
                    {
                        Tiles[7, 0] = 1;
                        Tiles[8, 0] = 1;
                        Top = false;
                    }
                }

                if (level.Rooms.FindAll(r => r.X == x && r.Y == y-1).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x && r.Y == y - 1).Top == true)
                    {
                        Tiles[7, 8] = 2;
                        Tiles[8, 8] = 2;
                        Bottom = true;
                    }
                    else
                    {
                        Tiles[7, 8] = 1;
                        Tiles[8, 8] = 1;
                        Bottom = false;
                    }
                }

                if (level.Rooms.FindAll(r => r.X == x + 1 && r.Y == y).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x + 1 && r.Y == y).Left == true)
                    {
                        Tiles[15, 4] = 2;
                        Right = true;
                    }
                    else
                    {
                        Tiles[15, 4] = 1;
                        Right = false;
                    }
                }

                if (level.Rooms.FindAll(r => r.X == x - 1 && r.Y == y).Count > 0)
                {
                    if (level.Rooms.Find(r => r.X == x - 1 && r.Y == y).Right == true)
                    {
                        Tiles[0, 4] = 2;
                        Left = true;
                    }
                    else
                    {
                        Tiles[0, 4] = 1;
                        Left = false;
                    }
                }                
              
                // Регистрируем команту в уровне
                level.Rooms.Add(this);

                if (Top == true && previousExit != ExitType.Down && level.Rooms.FindAll(r => r.X == x  && r.Y == y+1).Count== 0)
                  (new Room()).Generate(level, x, y + 1, Asset, limit - 1, ExitType.Up);

                if (Bottom == true && previousExit != ExitType.Up && level.Rooms.FindAll(r => r.X == x && r.Y == y -1).Count == 0)
                    (new Room()).Generate(level, x, y - 1, Asset, limit - 1, ExitType.Down);


                if (Left == true && previousExit != ExitType.Right && level.Rooms.FindAll(r => r.X == x-1 && r.Y == y ).Count == 0)
                    (new Room()).Generate(level, x - 1, y, Asset, limit - 1, ExitType.Left);


                if (Right == true && previousExit != ExitType.Left && level.Rooms.FindAll(r => r.X == x+1  && r.Y == y).Count == 0)
                    (new Room()).Generate(level, x + 1, y, Asset, limit - 1, ExitType.Right);

                
            }

            // Registering mob in the level
            Objects = new List<GameObjectInfo>();

            var content = GameServices.GetService<ContentManager>();
            MonsterData[] monsters = content.Load<SharedContent.MonsterData[]>("Monsters/Monster");
            ObjectData[]  items = content.Load<SharedContent.ObjectData[]>("Objects/Object");
            var mobNames = new List<string>();
            var itemNames = new List<string>();
            monsters.ToList().ForEach(d => mobNames.Add(d.ID));
            items.ToList().ForEach(d => itemNames.Add(d.Name));

            for (int col = 0; col < LevelConstants.Width; col++)
                for (int row = 0; row < LevelConstants.Height; row++)
                {                
                    
                    int tileId = Tiles[col, row];

                    if (tileId == 8)
                    {
                        int mobID = LevelConstants.Random.Next(mobNames.Count);
                        var info = new GameObjectInfo(GameObjectType.Mob, col, row, mobNames[mobID]);
                        Objects.Add(info);
                    }

                    if (tileId == 7 || tileId == 9)
                    {
                        int itemID = LevelConstants.Random.Next(itemNames.Count);
                        var info = new GameObjectInfo(GameObjectType.BasicItem, col, row, itemNames[itemID]);
                        Objects.Add(info);
                    }
                               
                    Layer1[col, row] = new Point(-1, -1);
                    Layer2[col, row] = new Point(-1, -1);

                    if (tileId == 0 || tileId == 7 || tileId == 9 || tileId == 8)
                        Layer1[col, row] = new Point(LevelConstants.Random.Next(3), 0);

                    if (tileId == 1)
                    {
                        Layer1[col, row] = new Point(LevelConstants.Random.Next(3), 2);
                        Layer2[col, row] = new Point(LevelConstants.Random.Next(3), 1);
                    }

                    if (tileId == 2)
                    {
                        Layer1[col, row] = new Point(0, 4);
                        Layer2[col, row] = new Point(0, 3);
                    }

                    if (tileId == 3)
                    {
                        Layer1[col, row] = new Point(1, 4);
                        Layer2[col, row] = new Point(1, 3);
                    }

                    if (tileId == 5)
                    {
                        Layer1[col, row] = new Point(0, 5);                        
                    }

                    if (tileId == 6)
                    {
                        Layer1[col, row] = new Point(0, 6);
                    }
                }
        }        
    }
}
