﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using SharedContent;
using Microsoft.Xna.Framework.Content;

namespace RLSA.LevelSystem
{
    class Level
    {
        public List<Room> Rooms;
        public string Asset;
        
        public Level(string asset)
        {
            Asset = asset;
            Rooms = new List<Room>();
            LevelConstants.LoadTemplates();

            // Запустить генератор уровня
            Generate();
        }

        public void Generate()
        {
            var initialRoom = new Room();
            initialRoom.Generate(this, 0, 0, Asset, 3, ExitType.None);
            ///  Rooms.Add(initialRoom);
        }
    }
}
