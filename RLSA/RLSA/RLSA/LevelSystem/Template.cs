﻿using SharedContent;
using System;

namespace RLSA.LevelSystem
{
    class Template
    {
        public int[,] Tiles;
        public bool Top = false;
        public bool Bottom = false;
        public bool Right = false;
        public bool Left = false;

        public Template(LevelData levelData)
        {   
            Tiles = new int[LevelConstants.Width, LevelConstants.Height];

            // Цикл по строкам
            for(int row = 0; row < LevelConstants.Height; row++)
            {
                string rowStr = levelData.TileString[row];
                var strings = rowStr.Split(' ');

                for (int col = 0; col < LevelConstants.Width; col++)
                {
                    int tileId = Convert.ToInt16(strings[col]);
                    Tiles[col, row] = tileId;

                    if (col == 7 && row == 0 && tileId == 2) Top = true;
                    if (col == 7 && row == 8 && tileId == 2) Bottom = true;
                    if (col == 0 && row == 4 && tileId == 2) Left = true;
                    if (col == 15 && row == 4 && tileId == 2) Right = true;
                }
            }
        }
    }
}
