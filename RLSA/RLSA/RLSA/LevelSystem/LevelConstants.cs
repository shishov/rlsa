﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShEngine;
using Microsoft.Xna.Framework.Content;
using SharedContent;

namespace RLSA.LevelSystem
{
    static class LevelConstants
    { 
        public static int Width = 16;
        public static int Height = 9;
        public static List<Template> Templates;
        public static Random Random = new Random();
        public static void LoadTemplates() 
        {
            var content = GameServices.GetService<ContentManager>();
            LevelData[] data = content.Load<SharedContent.LevelData[]>("Levels/Level");
            Templates = new List<Template>();

            foreach(var levelData in data)
                Templates.Add(new Template(levelData));
        }
       
    }
}
