﻿namespace SharedContent
{
    public class SkillData
    {
        public string Name;
        public string RequiredSkill;
        public int HealthBooster;
        public int EnergyBooster;
        public int DamageBooster;
        public int ArmorBooster;
        public int AccuracyBooster;
        public int SpeedBooster;
        public double CritChanceBooster;
        public double CritValueBooster;
        public double VampirismBooster;
        public int RageBooster;
        public double EssentionHarvestBooster;
        public double ExpHarvestBooster;
        public double NoManaArtefactBooster;
        public int Cost;
        public int IconID; 
    }
}
