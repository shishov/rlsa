﻿using Microsoft.Xna.Framework;

namespace SharedContent
{
    public class AnimationData
    {
        public float FrameRate;
        public bool Repeat;
        public string AnimationType;
        public string TextureID;
        public string[] Frames;
    }
}
