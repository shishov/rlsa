﻿namespace SharedContent
{
    public class ArmorUpgradeData
    {
        public string Name;
        public int ArmorBoost;
        public int SpeedBoost;
        public int Cost;
        public int UpgradeLevel;
    }
}
