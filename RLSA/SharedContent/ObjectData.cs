﻿using Microsoft.Xna.Framework;
namespace SharedContent
{
    public class ObjectData
    {
        public string Name;
        public bool Destructable;
        public int Health;
        public string RoomType;
        public Rectangle CropRect;
        public Vector2 Size;
        public Vector2 ImageOffset;
    }
}
