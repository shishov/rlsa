﻿using Microsoft.Xna.Framework;
namespace SharedContent
{
    public class MonsterData
    {
        public string ID;
        public int Damage;
        public int FireRate;
        public int Exp;
        public int HP;
        public int Speed;
        //public float Chance;
        public int BulletID;
        public int Type;
        public string Asset;
        public Vector2 Size;
        public Vector2 ImageOffset;
    }
}

