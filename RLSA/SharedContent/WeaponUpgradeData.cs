﻿namespace SharedContent
{
    public class WeaponUpgradeData
    {
        public string WeaponType;
        public string Type;
        public int DamageBooster;
        public int FireRateBooster;
        public int Cost;
        public int UpgradeLevel; // For tree-view, each weapon has his Levels
    }
}
