﻿namespace SharedContent
{
    public class WeaponData
    {
        public string Name;
        public int Damage;
        public int FireRate;
        public int Cost;
        public int BulletID;
        public int IconID;
    }
}
