﻿namespace SharedContent
{
    public class ArmorData
    {
        public string Name;
        public int ArmorBoost;
        public int SpeedBoost;
        public int Cost;
        public int IconID;
    }
}
